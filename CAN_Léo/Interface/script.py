# -*- coding: utf-8  -*-

import serial

try :
	ser = serial.Serial(port="COM6")
	print("Ouverture du port série ok")
except:
	print("Echec ouverture port série")


my_str = "0x150"
my_str_as_bytes = str.encode(my_str)

try :
	ser.write(my_str_as_bytes)
	print("Ecriture")
except:
	print("Echec Ecriture")