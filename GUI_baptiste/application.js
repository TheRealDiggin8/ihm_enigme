countdownManager = {
    // Configuration
    targetTime: new Date(new Date().getTime()+10*60000), // Date cible du compte à rebours (00:00:00)dt.getTime() + minutes*60000
    displayElement: { // Elements HTML où sont affichés les informations
        //day:  null,
        //hour: null,
        min:  null,
        sec:  null
    },
     
    // Initialisation du compte à rebours (à appeler 1 fois au chargement de la page)
    init: function(){
        // Récupération des références vers les éléments pour l'affichage
        // La référence n'est récupérée qu'une seule fois à l'initialisation pour optimiser les performances
        //this.displayElement.day  = jQuery('#countdown_day');
        //this.displayElement.hour = jQuery('#countdown_hour');
        this.displayElement.min  = jQuery('#countdown_min');
        this.displayElement.sec  = jQuery('#countdown_sec');
        this.targetTime = new Date(new Date().getTime()+10*60000);
         
        // Lancement du compte à rebours
        this.tick(); // Premier tick tout de suite
        window.setInterval("countdownManager.tick();", 1000); // Ticks suivant, répété toutes les secondes (1000 ms)
    },
     
    // Met à jour le compte à rebours (tic d'horloge)
    tick: function(){
        // Instant présent
        var timeNow  = new Date();
         
        // On s'assure que le temps restant ne soit jamais négatif (ce qui est le cas dans le futur de targetTime)
        if( timeNow > this.targetTime ){
            timeNow = this.targetTime;
        }
         
        // Calcul du temps restant
        var diff = this.dateDiff(timeNow, this.targetTime);
         
        //this.displayElement.day.text(  diff.day  );
        //this.displayElement.hour.text( diff.hour );
        if(diff.min == 0 && diff.sec == 0) partie_fini();
        this.displayElement.min.text(  diff.min  );
        this.displayElement.sec.text(  diff.sec  );
    },
     
    // Calcul la différence entre 2 dates, en jour/heure/minute/seconde
    dateDiff: function(date1, date2){
        var diff = {}                           // Initialisation du retour
        var tmp = date2 - date1;
 
        tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
        diff.sec = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
        diff.day = tmp;
 
        return diff;
    }
};






window.onload = function() {
    document.getElementById('gif_click').addEventListener('click', function (e) {
        if(document.getElementById('username').value == "") alert('veuillez rentrer votre nom d\'utilisateur');
        else {
            var cook = document.getElementById('username').value;
            var variable = "username=";
            document.cookie = variable.concat('',cook);
            
            document.getElementById('timer').innerHTML='<div id="countdown"><strong>Temps restant</strong> :<span id="countdown_min" >--</span> minutes <span id="countdown_sec" >--</span> secondes</div>';
            // mettre le timer de manière mieux parce que la c'est vraiment de la merde zebi
            countdownManager.init();
            document.getElementById('contenu_page').innerHTML="<h3 class='text-center display-5 font-weight-bold'>Voici le jeu du Labyrinthe ! Arriverez vous jusqu'a la bombe ?</h3><br><img id='choix_labyrinthe' src='images/choix_voies.png' class='rounded mx-auto d-block' />";
            ajax_function_simple('Piste1');
        }

      
      
    });
};

function ajax_function_simple(value){
    $.ajax({
        url:'ajax_php.php',
        type : 'POST',
        data : 'value=' + value  ,
        dataType : 'html',
        success : function(data){
            console.log(data);
            enigme(data);
        }

    });

}




function enigme(response){
    switch(response){
        case '2':
            document.getElementById('contenu_page').innerHTML="<h3 class='text-center display-5 font-weight-bold'>Pour deverouiller le coffre faites attention a votre doigtée</h3><br><img id='arriver_coffre' src='images/coffre_fort.png' class='rounded mx-auto d-block' />";
            ajax_function(2,'enigme_number');
            break;
        case '3':
            document.getElementById('contenu_page').innerHTML="<h3 class='text-center display-5 font-weight-bold'>Attention ! Les terroristes communiquent !</h3><br><img id='terreur' src='images/terroriste.png' class='rounded mx-auto d-block' /><br><div class='form-group'><label for='message' class='form-label mt-4'>Message capté</label><br><input type='text' class='form-control' id='message' aria-describedby='emailHelp' placeholder='Message captée'></div>";
            ajax_function(3,'enigme_number');
            break;
        case '4':
            break;
        case '5':
            break;
        case '6':
            break;
        case '7':
            break;
    }
}

function ajax_function(value,champs,username){
    var data_value;
    if(username != '') data_value = 'value=' + value + '&champs=' + champs+ '&username=' + username;
    else data_value = 'value=' + value + '&champs=' + champs;
    $.ajax({
        url:'ajax_php.php',
        type : 'POST',
        data : data_value ,
        dataType : 'html',
        success : function(data){
            console.log(data);
            enigme(data);
        }

    });

}


